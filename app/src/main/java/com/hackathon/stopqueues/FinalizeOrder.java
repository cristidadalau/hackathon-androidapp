package com.hackathon.stopqueues;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FinalizeOrder extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finalize_order);

        Button clickButton = (Button) findViewById(R.id.finalizeOrder);
        clickButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ScannedProductIntent = new Intent(FinalizeOrder.this, DecoderActivity .class);
                FinalizeOrder.this.startActivity(ScannedProductIntent);
            }
        });
    }
}
