package com.hackathon.stopqueues;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ProductListAdapter extends  BaseAdapter implements ListAdapter {

    Context context;
    JSONArray data;
    private static LayoutInflater inflater = null;

    public ProductListAdapter(Context context, JSONArray data) {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override public int getCount() {

        return data.length();
    }

    @Override public JSONObject getItem(int position) {

        return data.optJSONObject(position);
    }

    @Override public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);

        return jsonObject.optLong("id");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.product_row, null);

        JSONObject productObject = getItem(position);

        try {
            TextView productName = (TextView) vi.findViewById(R.id.productName);
            productName.setText(productObject.getString("productName"));

            TextView productPrice = (TextView) vi.findViewById(R.id.productPrice);

            float productPriceValue = Float.parseFloat(productObject.getString("productPrice"));
            productPrice.setText(String.valueOf(productPriceValue));

            if (productPriceValue  < 0) {
                productPrice.setTextColor(Color.parseColor("#FF0000"));
            }
        } catch (JSONException e){

        }

        return vi;
    }
}
