package com.hackathon.stopqueues;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProductActivity extends AppCompatActivity {

    public JSONArray currentProducts = new JSONArray();

    public String baseURl = "http://hackathon.dev";

    private JSONArray concatArray(JSONArray arr1, JSONArray arr2)
            throws JSONException {
        JSONArray result = new JSONArray();
        for (int i = 0; i < arr1.length(); i++) {
            result.put(arr1.get(i));
        }
        for (int i = 0; i < arr2.length(); i++) {
            result.put(arr2.get(i));
        }
        return result;
    }

    protected JSONArray identifyProduct(String scannedBarcode) {
        String url =this.baseURl + "/api/scan-barcode/"+scannedBarcode+"/1";
        UrlRequest urlRequest = new UrlRequest();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        try {
            String response = urlRequest.sendGet(url);

            JSONObject jsonObject = new JSONObject(response);
            JSONObject data = jsonObject.getJSONObject("data");

            JSONArray products = data.getJSONArray("products");

            this.currentProducts = this.concatArray(this.currentProducts, products);
            return this.currentProducts;
        } catch (Exception e) {
            String ex = e.getMessage();
        }

        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        Intent  intent = getIntent();
        String barcode = intent.getStringExtra("productBarcode");
        String scannedProducts = intent.getStringExtra("scannedProducts");
        try {
            if (scannedProducts != null) {
                JSONObject jsonbject = new JSONObject(scannedProducts);
            }

        }catch (JSONException $e) {

        }


        JSONArray productData = this.identifyProduct(barcode);
        ListView listview = (ListView) findViewById(R.id.productListView);
        listview.setAdapter(new ProductListAdapter(this, productData));

        Button finalizeOrderButton = (Button) findViewById(R.id.finalizeOrder);
        finalizeOrderButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ScannedProductIntent = new Intent(ProductActivity.this, FinalizeOrder .class);
                ProductActivity.this.startActivity(ScannedProductIntent);
            }
        });

        Button scanProductButton = (Button) findViewById(R.id.scanProduct);
        scanProductButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ScannedProductIntent = new Intent(ProductActivity.this, DecoderActivity .class);
                ScannedProductIntent.putExtra("scannedProducts", ProductActivity.this.currentProducts.toString());
                ProductActivity.this.startActivity(ScannedProductIntent);
            }
        });
    }
}
